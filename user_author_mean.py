import numpy as np
import pandas as pd

from pyecharts.charts import *
from pyecharts import options as opts

from sklearn.cluster import KMeans
import joblib
from sklearn import metrics
from scipy.spatial.distance import cdist

user_feature = pd.read_csv('用户特征.csv', index_col=0)
author_feature = pd.read_csv('作者特征.csv', index_col=0)

user_data = user_feature[(user_feature['完整观看数']>=1)&(user_feature['浏览量']>=5)]
print(len(user_data)/len(user_feature))

author_data = author_feature[(author_feature['总观完量']>=1)&(author_feature['总浏览量']>=3)]
print(len(author_data)/len(author_feature))

def km(data, name):
    K = range(2, 10) # K值选取范围
    X = data # 数据
    # scores = { 'SSE': [], 'sc': [], 'sse': []}
    scores = {'sc': [], 'sse': []}
    for _k in K:
        # 初始化模型并进行聚类 - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        kmeans = KMeans(n_clusters=_k, init='k-means++', random_state=0)
        kmeans.fit(X)
        _y = kmeans.predict(X) # 预测结果
        # 计算模型评估指标 - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        sse = sum(np.min(cdist(X,kmeans.cluster_centers_,'euclidean'),axis=1))/X.shape[0]
        sc = metrics.silhouette_score(X, _y) # 计算轮廓系数
        joblib.dump(kmeans, f'{name}{_k}聚类.model')
        # 储存评估值 - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        # scores['SSE'].append(SSE)
        scores['sse'].append(sse)
        scores['sc'].append(sc)
        print(f'聚{_k}类计算完成', end='\t')
    joblib.dump(scores, f'{name}聚类指标.score')
    print('指标储存完毕')
    return scores

def draw(k, sse, sc):
    chart = (
        Line(init_opts=opts.InitOpts(
            theme='light',
            width='350px',
            height='350px'
        ))
        .add_xaxis(k)
        .add_yaxis('sse', sse, yaxis_index=0, label_opts=opts.LabelOpts(is_show=False))
        .add_yaxis('sc', sc, yaxis_index=1, label_opts=opts.LabelOpts(is_show=False))
        .extend_axis(yaxis=opts.AxisOpts())
        .set_global_opts(
            title_opts=opts.TitleOpts(title='聚类效果'),
            xaxis_opts=opts.AxisOpts(type_="category", boundary_gap=True),
            yaxis_opts=opts.AxisOpts(
                type_="value",
                axistick_opts=opts.AxisTickOpts(is_show=True),
                splitline_opts=opts.SplitLineOpts(is_show=True),
            ),
        )
    )
    return chart
user_score = km(user_data, '用户')
print(user_score)

user_score =  joblib.load(f'用户聚类指标.score')
draw([str(x) for x in range(2,10)], user_score['sse'], user_score['sc']).render_notebook()