参考网址：`https://www.heywhale.com/mw/project/670e611dac73e0dd7e24142c`
### 1、特征构建
抖音用户浏览行为数据集包括以下内容：
用户（用户ID、用户ip）、观看作者（作者ID、作者ip）、什么视频(作品ID、作品频道、音乐ID、发布时间、发布具体时间、作品时长)、是否看完、是否点赞。

#### （1）将这些浏览行为进行分类：
1、用户信息：用户、用户ip
2、作品信息：作者ID、作者ip、作品频道、音乐ID、发布时间、发布具体时间、作品时长
3、作者信息：是否看完、是否点赞
#### （2）查看数据集内容
使用pandas库的read_csv()来查看数据集
```python 
df = pd.read_csv('./douyin_dataset.csv')
print(df.head())
```
#### （3）从用户、作者、作品角度构建特征指标
##### 用户角度
站在用户的角度，涉及到浏览量，点赞量，浏览的作品、作者、BGM的总数等
```python
print("正在用户特征分析.....")
user_df = pd.DataFrame()
user_df['uid'] = df.groupby('uid')['like'].count().index.tolist() # 将所有用户的uid提取为uid列
user_df.set_index('uid', inplace=True) # 设置uid列为index，方便后续数据自动对齐
user_df['浏览量'] = df.groupby('uid')['like'].count() # 统计对应uid下的浏览量
user_df['点赞量']  = df.groupby('uid')['like'].sum() # 统计对应uid下的点赞量
user_df['观看作者数'] = df.groupby(['uid']).agg({'author_id':pd.Series.nunique}) # 观看作者数
user_df['观看作品数'] = df.groupby(['uid']).agg({'item_id':pd.Series.nunique}) # 观看作品数
user_df['观看作品平均时长'] = df.groupby(['uid'])['duration_time'].mean() # 浏览作品平均时长
user_df['观看配乐数'] = df.groupby(['uid']).agg({'music_id':pd.Series.nunique}) # 观看作品中配乐的数量
user_df['完整观看数']  = df.groupby('uid')['finish'].sum() # 统计对应uid下的完整观看数
# 统计对应uid用户去过的城市数量
user_df['去过的城市数'] = df.groupby(['uid']).agg({'user_city':pd.Series.nunique})
# 统计对应uid用户看的作品所在的城市数量
user_df['观看作品城市数'] = df.groupby(['uid']).agg({'item_city':pd.Series.nunique})
user_df.describe()
user_df.to_csv('用户特征.csv', encoding='utf_8_sig')
print("用户特征分析完毕")
```
##### 作者角度
总浏览量，总点赞量
```python
author_df = pd.DataFrame()
author_df['author_id'] = df.groupby('author_id')['like'].count().index.tolist()
author_df.set_index('author_id', inplace=True)
author_df['总浏览量'] = df.groupby('author_id')['like'].count()
author_df['总点赞量']  = df.groupby('author_id')['like'].sum()
author_df['总观完量']  = df.groupby('author_id')['finish'].sum()
author_df['总作品数'] = df.groupby('author_id').agg({'item_id':pd.Series.nunique})

item_time = df.groupby(['author_id', 'item_id']).mean().reset_index()
author_df['作品平均时长'] = item_time.groupby('author_id')['duration_time'].mean()

author_df['使用配乐数量'] = df.groupby('author_id').agg({'music_id':pd.Series.nunique})
author_df['发布作品日数'] = df.groupby('author_id').agg({'real_time':pd.Series.nunique})

# pd.to_datetime(df['date'].max()) - pd.to_datetime(df['date'].min()) # 作品时间跨度为40，共计40天
author_days = df.groupby('author_id')['date']
_ = pd.to_datetime(author_days.max()) - pd.to_datetime(author_days.min())
author_df['创作活跃度(日)'] = _.astype('timedelta64[D]').astype(int) + 1
author_df['去过的城市数'] = df.groupby(['author_id']).agg({'item_city':pd.Series.nunique})
author_df.describe()

author_df.to_csv('作者特征.csv', encoding='utf_8_sig')

```
##### 作品角度
点赞量、浏览量、背景音乐、发布城市等
```python
item_df = pd.DataFrame()
item_df['item_id'] = df.groupby('item_id')['like'].count().index.tolist()
item_df.set_index('item_id', inplace=True)
item_df['浏览量'] = df.groupby('item_id')['like'].count()
item_df['点赞量']  = df.groupby('item_id')['like'].sum()
item_df['发布城市'] = df.groupby('item_id')['item_city'].mean()
item_df['背景音乐'] = df.groupby('item_id')['music_id'].mean()
item_df.to_csv('作品特征.csv', encoding='utf_8_sig')
```
