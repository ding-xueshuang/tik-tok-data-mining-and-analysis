import pandas as pd
import numpy as np

from pyecharts.charts import *
from pyecharts import options as opts

def line_chart(t, data):
    chart = (
        Line(init_opts = opts.InitOpts(theme='light', width='500px', height='300px'))
        .add_xaxis([i[0] for i in data])
        .add_yaxis(
            '',
            [i[1] for i in data],
            is_symbol_show=False,
            areastyle_opts=opts.AreaStyleOpts(opacity=1, color="cyan")
        )
        .set_global_opts(
            title_opts=opts.TitleOpts(title=t),
            xaxis_opts=opts.AxisOpts(type_="category", boundary_gap=True),
            yaxis_opts=opts.AxisOpts(
                type_="value",
                axistick_opts=opts.AxisTickOpts(is_show=True),
                splitline_opts=opts.SplitLineOpts(is_show=True),
            ),
        )
    )
    return chart
def pie_chart(t, data_pair):
    # 新建一个饼图
    chart = (
        Pie(init_opts=opts.InitOpts(theme='light', width='550px', height='300px'))
        .add('', data_pair ,radius=["30%", "45%"], # 半径范围，内径和外径
            label_opts=opts.LabelOpts(formatter="{b}: {d}%") # 标签设置，{d}表示显示百分比
        )
        .set_global_opts(
            title_opts=opts.TitleOpts(
                title=t
             ),
            legend_opts=opts.LegendOpts(pos_left="0%",pos_top="55",orient='vertical')
        )
    )
    return chart

# 用户浏览情况可视化

user_df = pd.read_csv('用户特征.csv')
user_df.head()

bins = [0, 5, 10, 25, 50, 100, 1951]
data = pd.cut(user_df['浏览量'], bins, right=True,labels=[f'({bins[x]}，{bins[x+1]}]' for x in range(len(bins)-1)]).value_counts().reset_index().values.tolist()
chart_cake=pie_chart('不同浏览量用户占比', data)
chart_cake.render('不同用户浏览情况图.html')

